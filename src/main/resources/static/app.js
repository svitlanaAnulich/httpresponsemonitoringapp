var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
        $("#set-url").show();
    }
    else {
        $("#conversation").hide();
        $("#set-url").hide();
    }
    $("#request-info").html("");
}

function connect() {
    var socket = new SockJS('/gs-guide-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/request-info', function (requestInfo) {
            showRequestInfo(JSON.parse(requestInfo.body));
        });

        stompClient.subscribe('/topic/error', function (error) {
            alert("Error " + error.body);
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendUrlName() {
    stompClient.send("/app/add-url", {}, $("#url-name").val());
}

function showRequestInfo(responseInfo) {
    var table = document.getElementById("request-info");
    var change = false;
    for (var i = 0, row; row = table.rows[i]; i++) {
        if (row.cells[0].innerText === responseInfo.url) {
            row.cells[1].innerText = responseInfo.time;
            row.cells[2].innerText = responseInfo.code;
            row.cells[3].innerText = responseInfo.length;
            row.cells[4].innerText = Number(row.cells[4].textContent)+1;
            change = true;
        }
    }
    if (!change) {
        $("#request-info").append("<tr>" +
            "<td>" + responseInfo.url + "</td>" +
            "<td>" + responseInfo.time + "</td>" +
            "<td>" + responseInfo.code + "</td>" +
            "<td>" + responseInfo.length + "</td>" +
            "<td> 0 </td>" +
            "</tr>");
    }
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendUrlName(); });
});