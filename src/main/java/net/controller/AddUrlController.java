package net.controller;

import net.exeption.ClientShowException;
import net.model.ResponseInfo;
import net.service.HttpRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import org.springframework.messaging.simp.SimpMessageSendingOperations;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Controller
@EnableScheduling
public class AddUrlController {

    @Autowired
    HttpRequestService httpRequestService;

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    private String sessionId;

    @MessageMapping("/add-url")
    @SendTo("/topic/request-info")
    public ResponseInfo requestInfo(String url, SimpMessageHeaderAccessor headerAccessor) throws ClientShowException {

        sessionId = headerAccessor.getSessionAttributes().get("sessionId").toString();
        System.out.println("sessionId="+sessionId);

        httpRequestService.validationUrl(url, sessionId);
        ResponseInfo responseInfo = new ResponseInfo();
        responseInfo.setUrl(url.trim());

        getInfo(url);

        return responseInfo;
    }

    private void getInfo(String url) {
        CompletableFuture.runAsync(() -> {
            ResponseInfo responseInfo = httpRequestService.getResponseInfo(url);
            messagingTemplate.convertAndSend("/topic/request-info", responseInfo);
        });
    }

    @MessageExceptionHandler
    @SendTo("/topic/error")
    public String handleException(ClientShowException exception) {
        return exception.getMessage();
    }


    @Scheduled(fixedDelay = 30000)
    private void updateResponseInfo(){
        List<String> urlList = httpRequestService.getLinkCacheForSession(sessionId);
        if (urlList != null) {
            for (String url :urlList) {
                getInfo(url);
            }
        }
    }

}
