package net.service;

import net.exeption.ClientShowException;
import net.model.ResponseInfo;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

@Service
public class HttpRequestService {

    private Map<String, List<String>> linkCache = new HashMap<>();

    public ResponseInfo getResponseInfo(String urlString) throws ClientShowException {

        try {
            URL url = new URL(urlString);
            try {
                ResponseInfo responseInfo = new ResponseInfo();
                long startTime = System.currentTimeMillis();
                HttpURLConnection http = (HttpURLConnection)url.openConnection();
                int statusCode = http.getResponseCode();
                responseInfo.setCode(statusCode);

                long length = Long.parseLong(http.getHeaderField("Content-Length"));
                responseInfo.setLength(length);

                long endTime = System.currentTimeMillis();
                responseInfo.setTime(endTime-startTime);
                responseInfo.setUrl(urlString.trim());

                return  responseInfo;

            } catch (IOException e) {
                throw new ClientShowException("Can't open connection to the url="+urlString);
            }
        } catch (MalformedURLException e) {
            throw new ClientShowException("String "+urlString+" is'n a url");
        }

    }

    private void addUrlToCache(String sessionId, String url){
        List<String> urlList = linkCache.get(sessionId);
        if (urlList == null) {
            urlList = new LinkedList<>();
        }
        urlList.add(url);
        linkCache.put(sessionId,urlList);
        
    //    outChache();
    }

    private void outChache() {
        for (Map.Entry entry : linkCache.entrySet()) {
            System.out.println(entry.getValue().toString());
        }
    }

    public List<String> getLinkCacheForSession(String sessionId) {
        return linkCache.get(sessionId);
    }

    public void validationUrl(String urlString, String sessionId) throws ClientShowException {
        try {
            URL url = new URL(urlString);
            addUrlToCache(sessionId, urlString.trim());
        } catch (IOException e) {
            throw new ClientShowException("Can't open connection to the url="+urlString);
        }
    }
}
