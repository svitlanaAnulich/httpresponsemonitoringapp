package net.exeption;

public class ClientShowException extends RuntimeException {

    public ClientShowException(String message) {
        super(message);
    }

}
