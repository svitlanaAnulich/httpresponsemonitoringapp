package net.model;

public class ResponseInfo {

    private String url;
    private long time;
    private int code;
    private long length;

    public ResponseInfo() {
    }

    public ResponseInfo(String url, long time, int code, long length) {
        this.url = url;
        this.time = time;
        this.code = code;
        this.length = length;
    }

    public String getUrl() {
        return url;
    }

    public long getTime() {
        return time;
    }

    public int getCode() {
        return code;
    }

    public long getLength() {
        return length;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setLength(long length) {
        this.length = length;
    }
}
